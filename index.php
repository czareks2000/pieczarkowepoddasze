<?php
	include("config.php");
	

	if(isset($_GET['page']))
	{
		$url = $_GET['page'];
	}
	else
	{
		$url = START;
	}
	
	$file = "modules/".$url.".php";
	
	$title = $page_name[$url];

	include("modules/header.php");
	
	if(file_exists($file))
	{
		include($file);
	}
	else
	{
		include("modules/start.php");
	}
	
	include("modules/footer.php");
?>