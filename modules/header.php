<!DOCTYPE HTML>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title><?=$title?></title>
		<meta name="description" content='Strona klubu "Pieczarkowe Poddasze"'>
		<meta name="keywords" content="planszowki, pieczarek">
		<meta name="author" content="Cezary Stachurski">
		<meta http-equiv="X-Ua-Compatible" content="IE=edge">

		<link rel="shortcut icon" type="image/x-icon" href="img/logo-color.png">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/main.css">
		<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,700&amp;subset=latin-ext" rel="stylesheet">

		<script
  src="https://code.jquery.com/jquery-3.3.1.js"
  integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60="
  crossorigin="anonymous"></script>

		<script src="js/jquery.scrollTo.min.js"></script>

		<script>
			jQuery(function($)
			{
				//zresetuj scrolla
				$.scrollTo(0);

				$('#link1').click(function(){ $.scrollTo($('#historia'),500); });
				$('#link2').click(function(){ $.scrollTo($('#czlonkowie'),1000); });
				$('#link3').click(function(){ $.scrollTo($('#gry'),1000); });

				$('.scrollup').click(function()
				{
					$('html, body').animate({scrollTop : 0},500);
					return false;
				});
			}
			);

			(function($){
				$(window).scroll(function()
				{
					if($(this).scrollTop()>300) $('.scrollup').fadeIn();
					else $('.scrollup').fadeOut();
				}
				);
			})(jQuery);
		</script>


	</head>
	<body>
		<a href="#" class="scrollup"></a>
		<header>
			<div >
				<nav class="navbar navbar-dark bg-primary navbar-expand-md">
					<div class="container">
						<a  class="navbar-brand" href="start"><img src="img/logo-color.png" class="d-inline-block align-bottom" width="30" alt=""> Pieczarkowe Poddasze</a>

						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#topnav" aria-controls="topnav" aria-expanded="false" aria-label="Przełącznik nawigacji">
							<span class="navbar-toggler-icon"></span>
						</button>

						<div id="topnav" class="collapse navbar-collapse">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item"><a class="nav-link" href="start">Start</a></li>
								<li class="nav-item"><a class="nav-link" href="o-klubie">O klubie</a></li>
								<li class="nav-item"><a class="nav-link" href="zobacz-zdjecia">Galeria</a></li>
								<li class="nav-item"><a class="nav-link" href="dolacz-do-klubu">Kontakt</a></li>
							</ul>
						</div>
					</div>
				</nav>
			</div>
		</header>
