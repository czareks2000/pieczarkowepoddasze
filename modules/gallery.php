<aside style="padding-top:30px"></aside>
<main>

	<article>
		<div class="container images d-block mr-auto ml-auto">
			<div class="row">
				<?php

					require_once"connect.php";

					$conn = @new MySQLi($host, $db_user, $db_password, $db_name);


					if($conn->connect_errno!=0)
					{
						echo "Error: ".$conn->connect_errno;
					}
					else
					{
						mysqli_set_charset($conn,"utf8");

						$sql = "SELECT * FROM zdjecia";

						$result = $conn->query($sql);


						while($row = $result->fetch_assoc())
						{

							echo '<div class="col-md-6 col-lg-4">';
							echo '		<a href="'.$row["fullsize"].'" target="_blank"><img src="'.$row["regular"].'" alt="'.$row["podpis"].'"></a>';
							echo '</div>';
						}


						$result->close();
						$conn->close();
					}

				?>
	
			</div>
		</div>
	</article>

</main>
