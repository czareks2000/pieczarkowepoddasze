<main>
	<div class="container">
		<article class="article lightbg">

			<header>
				<h1>Witaj na "Pieczarkowym Poddaszu"!</h1>
			</header>
			<p>
				Nazywam się Cezary Stachurski i jestem założycielem klubu planszówkowego "Pieczarkowe Poddasze". Pierwszy człon pochodzi od mojej nazwy używanej w internecie, mianowicie "Pieczarek", a że pomieszczenie w którym odbywają się spotkania klubu to mój nieużywany strych, postanowiłem nazwać to właśnie "Pieczarkowe Poddasze".
			</p>
			<a href="o-klubie">Czytaj dalej...</a>

		</article>
	</div>
		<article class="article darkbg">

			<header>
				<h1>Nasza galeria zdjęć</h1>
			</header>

			<div class="container images d-block mr-auto ml-auto">
				<div class="row">
					<?php

						require_once"connect.php";

						$conn = @new MySQLi($host, $db_user, $db_password, $db_name);


						if($conn->connect_errno!=0)
						{
							echo "Error: ".$conn->connect_errno;
						}
						else
						{
							mysqli_set_charset($conn,"utf8");

							$sql = "SELECT * FROM zdjecia";

							$result = $conn->query($sql);

							$i = 0;

							while(($row = $result->fetch_assoc()) && ($i<3))
							{
								echo '<div class="col-md-4">';
								echo '		<a href="'.$row["fullsize"].'" target="_blank"><img src="'.$row["regular"].'" alt="'.$row["podpis"].'"></a>';
								echo '</div>';
								$i++;
							}

							$result->close();
							$conn->close();
						}

					?>
				</div>
			</div>

			<a href="zobacz-zdjecia">Zobacz więcej...</a>

		</article>
	<div class="container">
		<article class="article lightbg">

			<header>
				<h1>Dołącz do klubu!</h1>
			</header>
			<p>
				Chcesz pograć w planszówki ale nie masz gdzie i z kim? Ten klub jest właśnie dla ciebie. Wypełnij formularz i dołącz do klubu już dziś! Czeka na ciebie iście szampańska zabawa i grono interesujących ludzi!
			</p>
			<a href="dolacz-do-klubu">Formularz zgłoszeniowy</a>
	</div>
		</article>

</main>
