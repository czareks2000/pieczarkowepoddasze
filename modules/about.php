


<aside style="padding-top:90px"></aside>
<div class="container">
	<div class="row">

		<div class="col-md-9">

			<main>

				<article id="historia" class="blog-post">

					<div class="post-content">

						<header>
							<h1>HISTORIA</h1>
							<div class="line"></div>
							<div style="text-align: justify;">
							<p><p>Witam, nazywam się Cezary Stachurski i jestem założycielem klubu planszówkowego "Pieczarkowe Poddasze". Pierwszy człon pochodzi od mojej nazwy używanej w internecie, mianowicie "Pieczarek", a że pomieszczenie w którym odbywają się spotkania klubu to mój nieużywany strych, postanowiłem nazwać to właśnie "Pieczarkowe Poddasze".</p></p>

							<p>Skąd pomysł? Na początku razem ze znajomymi z technikum postanowiliśmy, że fajnie by było pograć sobie razem w planszówki. Mieliśmy zamiar złożyć się na pare tytułów i regularnie je ogrywać. W tym momencie pojawił się pierwszy problem - nie mieliśmy odpowiedniego miejsca, w którym regularnie można by spotykać się większą liczbą osób. Drugim problemem była organizacja, zazwyczaj przy takich ciekawych inicjatywach, każdy wyraża chęć uczestnictwa, ale nie każdemu chce się być organizatorem. W tym miejscu pojawiam się ja.</p>

							<p>Zastanawiając się nad miejscem, przypomniałem sobie, że "Hej, przecież mam wolne poddasze", wystarczyło trochę posprzątać:</p>

							<img class="d-block col-md-12 mr-auto ml-auto" src="img/strych.jpg" alt="Zdjęcie strychu">

							<p><p>Miejsce znalezione, to teraz jakieś gry by się przydały. Przekopałem cały dom i znalazłem trochę planszówek na dobry początek. Zaczęliśmy spotykać się w weekendy, każdy przyniósł jakąś grę i dalej jakoś poszło.</p></p>
							</div>
						</header>

					</div>

				</article>

				<article id="czlonkowie" class="blog-post">

					<div class="post-content">

						<header>

							<h1 >CZŁONKOWIE</h1>
							<div class="line"></div>
							<?php

								require_once"connect.php";

								$conn = @new MySQLi($host, $db_user, $db_password, $db_name);


								if($conn->connect_errno!=0)
								{
									echo "Error: ".$conn->connect_errno;
								}
								else
								{
									mysqli_set_charset($conn,"utf8");

							    $sql = "SELECT * FROM czlonkowie";

							    $result = $conn->query($sql);


									while($row = $result->fetch_assoc())
									{
											echo'<div class="member row">';
											echo'	<div class="col-lg-4">';
											echo'		<img src="'.$row["zdjecie"].'" class="d-block m-auto"alt="'.$row["imie"].' '.$row["nazwisko"].'">';
											echo'	</div>';
											echo'	<div class="col-lg-8">';
											echo'		<p class="membername"><u>'.$row["imie"].' '.$row["nazwisko"].'</u><br>';
											echo'		<i>'.$row["podpis"].'</i></p>';
											echo'		<p>'.$row["opis"].'</p>';
											echo'	</div>';
											echo'</div>';
							    }


									$result->close();
									$conn->close();
								}

							?>
						</header>

					</div>

				</article>

				<article  id="gry" class="blog-post">

					<div class="post-content">

						<header>

							<h1>POSIADANE GRY</h1>
							<div class="line"></div>
							<div style="text-align: justify;">
								<p><p>Aktualnie klub posiada natępujące gry:</p></p>
							</div>
							<?php

								require_once"connect.php";

								$conn = @new MySQLi($host, $db_user, $db_password, $db_name);


								if($conn->connect_errno!=0)
								{
									echo "Error: ".$conn->connect_errno;
								}
								else
								{
									mysqli_set_charset($conn,"utf8");

							    $sql = "SELECT * FROM planszowki";

							    $result = $conn->query($sql);

									echo '<ul>';
									while($row = $result->fetch_assoc())
									{
							        echo '<li><b>'.$row["nazwa"].'</b> - '.$row["opis"];
							    }
									echo '</ul>';

									$result->close();
									$conn->close();
								}

							?>
						</header>

					</div>

				</article>

			</main>

		</div>

		<aside class="d-none d-md-block col-md-3">

			<div class="side">

				<h2>Spis treści</h2>

				<ul class="circled" style="margin-bottom:16px;">
					<li><a id="link1" href="#">Historia</a></li>
					<li><a id="link2" href="#">Członkowie</a></li>
					<li><a id="link3" href="#">Posiadane gry</a></li>

				</ul>

			</div>

		</aside>

	</div>
</div>
<aside style="text-align:center; padding-top:30px;"></aside>
