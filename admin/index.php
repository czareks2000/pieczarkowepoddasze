<?php

	session_start();

	if(isset($_SESSION['zalogowany']) && $_SESSION['zalogowany'] == true)
	{
		header('Location: dashboard.php');
		exit();
	}
?>
<!DOCTYPE html>
<html lang="pl">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Logowanie</title>

    <meta http-equiv="X-Ua-Compatible" content="IE=edge">

		<link rel="shortcut icon" type="image/x-icon" href="../img/logo-color.png">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet/less" href="css/main.less">
		<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,700&amp;subset=latin-ext" rel="stylesheet">
		<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>

  </head>
  <body>
    <header>
			<div >
				<nav class="navbar navbar-dark bg-primary navbar-expand-md">
					<div class="container">
						<a  class="navbar-brand ml-auto mr-auto" href="../index.php"><img src="../img/logo-color.png" class="d-inline-block align-bottom" width="30" alt=""> Pieczarkowe Poddasze</a>
					</div>
				</nav>
			</div>
		</header>

<!-- -->
    <main>

      <div class="container">

        <header>
          <div class="login-form-header">Zaloguj się do panelu administratora!</div>
        </header>

        <article>
          <div class="row">
            <form class="login-from" action="login.php" method="post">
              <div class="form-group">
                <label for="usr">Login:</label>
                <input type="text" name="login" class="form-control" id="usr">
              </div>
              <div class="form-group mb-4">
                <label for="pwd">Hasło:</label>
                <input type="password" name="password" class="form-control" id="pwd">
              </div>
              <button type="submit" class="btn btn-primary btn-block">Zaloguj się</button>
							<?php
								if(isset($_SESSION['blad'])) echo $_SESSION['blad'];
							?>
            </form>
          </div>
        </article>

      </div>

    </main>

    <footer id="footer" class="fixed-bottom" style="margin-top: 37px;">
      <div class="container">
        Wszelkie prawa zastrzeżone &copy; 2018 Cezary Stachurski
      </div>
    </footer>



		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
		<script>
			(function($){
				$(window).on('resize', function()
				{
					if($(window).height()<480) $("#footer").removeClass("fixed-bottom");
					else $("#footer").addClass("fixed-bottom");;
				}
				);
			})(jQuery);
		</script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>

		<script src="../js/bootstrap.min.js"></script>

	</body>
</html>
