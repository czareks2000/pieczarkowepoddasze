<?php
	//main page
	define("START", "members");


	if(isset($_GET['page']))
	{
		$url = $_GET['page'];
	}
	else
	{
		$url = START;
	}

	$file = "modules/".$url.".php";

	include("modules/header.php");

	if(file_exists($file))
	{
		include($file);
	}
	else
	{
		include("modules/members.php");
	}

	include("modules/footer.php");
?>
