<?php
    session_start();

    if($_POST["podpis"] != "")
    {
      require_once"connect.php";

      $podpis = htmlentities($_POST["podpis"]);

      $target_dir_regular = "../img/regular/";
      $target_dir_fullsize = "../img/full-size/";

      $target_file_regular = $target_dir_regular . basename($_FILES["regular"]["name"]);
      $target_file_fullsize = $target_dir_fullsize . basename($_FILES["fullsize"]["name"]);

      $dir_regular = "img/regular/";
      $dir_fullsize = "img/full-size/";

      if (basename($_FILES["regular"]["name"]) != "" && basename($_FILES["fullsize"]["name"]) == "")
      {
        $lokalizacja_regular = $dir_regular . basename($_FILES["regular"]["name"]);
        $lokalizacja_fullsize = $dir_regular . basename($_FILES["regular"]["name"]);

      }
      elseif (basename($_FILES["regular"]["name"]) == "" && basename($_FILES["fullsize"]["name"]) != "")
      {
        $_SESSION["brakZdjecia"] = "Brak zdjęcia(szerokość 500px)!";
        $_SESSION["podpis"] = $_POST["podpis"];
        header('Location: zdjecia');
        exit();
      }
      elseif (basename($_FILES["regular"]["name"]) == "" && basename($_FILES["fullsize"]["name"]) == "")
      {
        $_SESSION["brakZdjecia"] = "Brak zdjęcia(szerokość 500px)!";
        $_SESSION["podpis"] = $_POST["podpis"];
        header('Location: zdjecia');
        exit();
      }
      else
      {
        $lokalizacja_regular = $dir_regular . basename($_FILES["regular"]["name"]);
        $lokalizacja_fullsize = $dir_fullsize . basename($_FILES["fullsize"]["name"]);
      }

      $conn = @new MySQLi($host, $db_user, $db_password, $db_name);

      if($conn->connect_errno!=0)
      {
        echo "Error: ".$conn->connect_errno;
      }
      else
      {
        mysqli_set_charset($conn,"utf8");

        $sql = "INSERT INTO zdjecia VALUES ('$podpis', '$lokalizacja_regular', '$lokalizacja_fullsize', NULL)";

        $conn->query($sql);

        $conn->close();

        move_uploaded_file($_FILES["regular"]["tmp_name"], $target_file_regular);
        move_uploaded_file($_FILES["fullsize"]["tmp_name"], $target_file_fullsize);

        unset($_SESSION["brakZdjecia"]);
        unset($_SESSION["podpis"]);
        header('Location: zdjecia');
      }
    }
    else
    {
      $_SESSION["brakZdjecia"] = "Brak podpisu!";
      header('Location: zdjecia');
    }

?>
