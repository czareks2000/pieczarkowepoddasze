<?php

  session_start();

  if((!isset($_POST['login'])) || (!isset($_POST['password'])))
  {
    header('Location: index.php');
    exit();
  }

  require_once"connect.php";

	$conn = @new MySQLi($host, $db_user, $db_password, $db_name);


	if($conn->connect_errno!=0)
	{
		echo "Error: ".$conn->connect_errno;
	}
	else
	{

		$login = $_POST['login'];
		$haslo = $_POST['password'];


		$login = htmlentities($login, ENT_QUOTES, "UTF-8");
		$haslo= htmlentities($haslo, ENT_QUOTES, "UTF-8");

    $login = mysqli_real_escape_string($conn,$login);
    $haslo = mysqli_real_escape_string($conn,$haslo);

    $sql = "SELECT * FROM users WHERE user='$login' AND password='$haslo'";

    $result = $conn->query($sql);


			if($result->num_rows > 0)
			{
				$_SESSION['zalogowany'] = true;

				$wiersz = $result->fetch_assoc();
				$_SESSION['user'] = $wiersz['user'];
        $_SESSION['name'] = $wiersz['name'];

				unset($_SESSION['blad']);

				$result->close();

				header('Location: dashboard.php');
			}
			else
			{
        $_SESSION['blad']='<div style="color:red; margin-top: 20px; text-align: center;">Nieprawidłowy login lub hasło!</div>';

				header('Location: index.php');
			}


		$conn->close();
	}

?>
