<?php
  session_start();

  if(($_POST["nazwa"] != "") && ($_POST["opis"] != "") && ($_POST["wlasciciel"] != ""))
  {
    require_once"connect.php";

    $nazwa = htmlentities($_POST["nazwa"]);
    $opis = htmlentities($_POST["opis"]);
    $wlasciciel = htmlentities($_POST["wlasciciel"]);

    $conn = @new MySQLi($host, $db_user, $db_password, $db_name);

    if($conn->connect_errno!=0)
    {
      echo "Error: ".$conn->connect_errno;
    }
    else
    {
      mysqli_set_charset($conn,"utf8");

      $sql = "INSERT INTO planszowki (nazwa, opis, wlasciciel) VALUES ('$nazwa', '$opis', '$wlasciciel')";

      $conn->query($sql);

      $conn->close();

      unset($_SESSION["blad"]);

      unset($_SESSION["graNazwa"]);
      unset($_SESSION["graOpis"]);
      unset($_SESSION["graWlasciciel"]);

    }
  }
  else{
    $_SESSION["blad"] = "Uzupełnij wszystkie pola!";

    $_SESSION["graNazwa"] = $_POST["nazwa"];
    $_SESSION["graOpis"] = $_POST["opis"];
    $_SESSION["graWlasciciel"] = $_POST["wlasciciel"];

  }
  header('Location: planszowki');
?>
