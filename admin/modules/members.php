<main>
  <div class="container">
    <div class="row">
      <div class="col-12">

          <article class="article">
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Imię</th>
                    <th>Nazwisko</th>
                    <th>Podpis</th>
                    <th>E-mail</th>
                    <th>Akcja</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                    require_once"connect.php";

                    $conn = @new MySQLi($host, $db_user, $db_password, $db_name);


                    if($conn->connect_errno!=0)
                    {
                      echo "Error: ".$conn->connect_errno;
                    }
                    else
                    {
                      mysqli_set_charset($conn,"utf8");

                      $sql = "SELECT * FROM czlonkowie";

                      $result = $conn->query($sql);


                      while($row = $result->fetch_assoc())
                      {
                        echo'<tr>';
                        echo'  <td>'.$row["imie"].'</td>';
                        echo'  <td>'.$row["nazwisko"].'</td>';
                        echo'  <td>'.$row["podpis"].'</td>';
                        echo'  <td>'.$row["email"].'</td>';
                        echo'  <td>';
                        echo'   <form action="deleteMember.php" method="post">';
                        echo'    <input type="hidden" value="'.$row["imie"].'" name="imie">';
                        echo'    <input type="hidden" value="'.$row["nazwisko"].'" name="nazwisko">';
                        echo'    <input type="hidden" value="'.$row["zdjecie"].'" name="zdjecie">';
                        echo'    <button type="submit" class="btn btn-secondary btn-sm">Usuń</button>';
                        echo'   </form>';
                        echo'  </td>';
                        echo'</tr>';
                      }


                      $result->close();
                      $conn->close();
                    }

                  ?>
                </tbody>
              </table>
            </div>
          </article>

          <article class="article">
            <h2>Dodaj nowego członka</h2>
            <div class="row">

              <div class="display-inline col-lg-6">
                <form action="addMember.php" method="post" enctype="multipart/form-data">

                  <div class="form-group row">
                    <label for="nowe-imie" class="col-2 col-form-label">Imię</label>
                    <div class="col-10">
                      <input class="form-control" value="<?php if(isset($_SESSION["imie"])) echo $_SESSION["imie"]?>" type="text" name="imie" id="nowe-imie">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nowe-nazwisko" class="col-2 col-form-label">Nazwisko</label>
                    <div class="col-10">
                      <input class="form-control" value="<?php if(isset($_SESSION["nazwisko"])) echo $_SESSION["nazwisko"]?>" type="text" name="nazwisko" id="nowe-nazwisko">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nowy-podpis" class="col-2 col-form-label">Podpis</label>
                    <div class="col-10">
                      <input class="form-control" value="<?php if(isset($_SESSION["podpis"])) echo $_SESSION["podpis"]?>" type="text" name="podpis" id="nowy-podpis">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nowy-email" class="col-2 col-form-label">E-mail</label>
                    <div class="col-10">
                      <input class="form-control" value="<?php if(isset($_SESSION["e-mail"])) echo $_SESSION["e-mail"]?>" type="text" name="e-mail" id="nowy-e-mail">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputFile" class="col-2 col-form-label">Zdjęcie</label>
                    <div class="col-10">
                      <input type="file" class="form-control-file" name="zdjecie" id="exampleInputFile" >
                    </div>
                  </div>
                </div>

                <div class="display-inline col-lg-6">
                  <div class="form-group">
                    <div class="col-12">
                      <textarea class="form-control" placeholder="Opis" name="opis" rows="8"><?php if(isset($_SESSION["opis"])) echo $_SESSION["opis"]?></textarea>
                    </div>
                  </div>
                </div>

              </div>

              <div class="offset-lg-3 col-lg-6">
                <button type="submit" class="btn btn-secondary btn-block">Dodaj</button>
              </div>
              <div class="error"><?php
                if(isset($_SESSION["polaMembers"]))
                {
                  echo $_SESSION["polaMembers"];
                }
              ?></div>
            </form>
          </article>

          <article class="article">
            <h2>Edytuj członka</h2>
            <form action="editMember.php" method="post" enctype="multipart/form-data">
              <div class="form-group row">
                <label class="col-form-label col-1" for="inlineFormCustomSelect">Wybierz:</label>
                <div class="col-11">
                  <select class="custom-select" name="id" id="inlineFormCustomSelect">
                    <?php

                      require_once"connect.php";

                      $conn = @new MySQLi($host, $db_user, $db_password, $db_name);


                      if($conn->connect_errno!=0)
                      {
                        echo "Error: ".$conn->connect_errno;
                      }
                      else
                      {
                        mysqli_set_charset($conn,"utf8");

                        $sql = "SELECT * FROM czlonkowie";

                        $result = $conn->query($sql);

                        while($row = $result->fetch_assoc())
                        {
                          echo'  <option value="'.$row["id"].'">'.$row["imie"].' '.$row["nazwisko"].'</option>';
                        }

                        $result->close();
                        $conn->close();
                      }

                    ?>
                   </select>
                </div>
              </div>
              <div class="row">

                <div class="display-inline col-lg-6">
                  <div class="form-group row">
                    <label for="nowe-imie" class="col-2 col-form-label">Imię</label>
                    <div class="col-10">
                      <input class="form-control" type="text" name="imie" id="nowe-imie">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nowe-nazwisko" class="col-2 col-form-label">Nazwisko</label>
                    <div class="col-10">
                      <input class="form-control" type="text" name="nazwisko" id="nowe-nazwisko">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nowy-podpis" class="col-2 col-form-label">Podpis</label>
                    <div class="col-10">
                      <input class="form-control" type="text" name="podpis" id="nowy-podpis">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="nowy-email" class="col-2 col-form-label">E-mail</label>
                    <div class="col-10">
                      <input class="form-control" type="text" name="email" id="nowy-email">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleInputFile" class="col-2 col-form-label">Zdjęcie</label>
                    <div class="col-10">
                      <input type="file" class="form-control-file" name="zdjecie" id="exampleInputFile" >
                    </div>
                  </div>
                </div>

                <div class="display-inline col-lg-6">
                  <div class="form-group">
                    <div class="col-12">
                      <textarea class="form-control" placeholder="Opis" name="opis" rows="8"></textarea>
                    </div>
                  </div>
                </div>

              </div>

              <div class="row">
                <div class="offset-lg-3 col-lg-6">
                  <button type="submit" class="btn btn-secondary btn-block">Zastosuj zmiany</button>
                </div>
              </div>
            </form>
          </article>

        </div>
      </div>
    </div>
  </div>
</main>
