<?php
	session_start();

	if(!isset($_SESSION['zalogowany']))
	{
		header('Location: index.php');
		exit();
	}

?>
<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Panel Administratora</title>

		<meta http-equiv="X-Ua-Compatible" content="IE=edge">

		<link rel="shortcut icon" type="image/x-icon" href="../img/logo-color.png">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="css/fontello.css">
		<link rel="stylesheet/less" href="css/main.less">
		<link href="https://fonts.googleapis.com/css?family=Titillium+Web:400,700&amp;subset=latin-ext" rel="stylesheet">
		<script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js" ></script>
		<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	</head>
	<body>

		<header>
			<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-primary p-0 shadow">
	      <a class="navbar-brand d-none d-sm-block col-sm-3 col-md-2 mr-0" href="../index.php"><img src="../img/logo-color.png" class="d-inline-block align-bottom" width="30" alt="">Pieczarkowe Poddasze</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			    <span class="navbar-toggler-icon"></span>
			  </button>
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
			    <ul class="navbar-nav d-block d-lg-none ml-auto">
			      <li class="nav-item ml-3">
							<a class="nav-link" href="czlonkowie">Członkowie</a>
			      </li>
						<li class="nav-item ml-3">
							<a class="nav-link" href="planszowki">Planszówki</a>
			      </li>
						<li class="nav-item ml-3">
							<a class="nav-link" href="czlonkowie">Zdjęcia</a>
			      </li>
						<li class="nav-item ml-3 text-nowrap">
		          <a class="nav-link" href="logout.php">Wyloguj się</a>
		        </li>
			    </ul>
			  </div>
				<ul class="navbar-nav d-none d-lg-block px-3">
	        <li class="nav-item text-nowrap">
	          <a class="nav-link" href="logout.php">Wyloguj się</a>
	        </li>
	      </ul>
	    </nav>
		</header>



		<!-- Side navigation -->
		<nav class="sidenav d-none d-lg-block">
		  <a href="czlonkowie"><span class="icon-user"></span>Członkowie</a>
		  <a href="planszowki"><span class="icon-briefcase"></span>Planszówki</a>
		  <a href="zdjecia"><span class="icon-folder-empty"></span>Zdjęcia</a>
		</nav>

		<div style="margin-top: 41px;"></div>
		<!-- Page content -->
		<div class="main">
