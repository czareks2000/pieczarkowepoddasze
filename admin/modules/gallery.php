<main>
  <div class="container">
    <div class="row">
      <div class="col-12">
          <article class="article">
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Podpis</th>
                    <th>Full size</th>
                    <th>Regular</th>
                    <th>Akcja</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

          					require_once"connect.php";

          					$conn = @new MySQLi($host, $db_user, $db_password, $db_name);


          					if($conn->connect_errno!=0)
          					{
          						echo "Error: ".$conn->connect_errno;
          					}
          					else
          					{
          						mysqli_set_charset($conn,"utf8");

          						$sql = "SELECT * FROM zdjecia";

          						$result = $conn->query($sql);


          						while($row = $result->fetch_assoc())
          						{
                        echo'<tr>';
                        echo'  <td>'.$row["podpis"].'</td>';
                        echo'  <td>'.$row["fullsize"].'</td>';
                        echo'  <td>'.$row["regular"].'</td>';
                        echo'  <td>';
                        echo'   <form action="deleteImage.php" method="post">';
                        echo'    <input type="hidden" value="'.$row["podpis"].'" name="podpis">';
                        echo'    <input type="hidden" value="'.$row["fullsize"].'" name="fullsize">';
                        echo'    <input type="hidden" value="'.$row["regular"].'" name="regular">';
                        echo'    <button type="submit" class="btn btn-secondary btn-sm">Usuń</button>';
                        echo'   </form>';
                        echo'  </td>';
                        echo'</tr>';
          						}


          						$result->close();
          						$conn->close();
          					}

          				?>
                </tbody>
              </table>
            </div>
          </article>
          <article class="article">
            <h2>Dodaj nowe zdjecie</h2>
            <form class="" action="addImages.php" method="post" enctype="multipart/form-data">
              <div class="col-lg-6 offset-lg-3">
                <div class="form-group row">
                  <label for="nowy-podpis" class="col-12 col-form-label">Podpis</label>
                  <div class="col-12">
                    <input class="form-control" value="<?php if(isset($_SESSION["podpis"])) echo $_SESSION["podpis"]?>" type="text" name="podpis" id="nowy-podpis">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="exampleInputFile" class="col-md-5 col-form-label">Zdjęcie(szerokość 500px)</label>
                  <div class="col-md-7">
                    <input type="file" class="form-control-file" name="regular" id="exampleInputFile" >
                  </div>
                </div>
                <div class="form-group row">
                  <label for="exampleInputFile" class="col-md-5 col-form-label">Full-size(opcjonalnie)</label>
                  <div class="col-md-7">
                    <input type="file" class="form-control-file" name="fullsize" id="exampleInputFile" >
                  </div>
                </div>

                <div class="offset-2 col-8">
                  <button type="submit" class="btn btn-secondary btn-block">Dodaj</button>
                </div>
              </div>
            </form>
            <div class="error"><?php
              if(isset($_SESSION["brakZdjecia"]))
              {
                echo $_SESSION["brakZdjecia"];
              }
            ?></div>
          </article>

        </div>
      </div>
    </div>
  </div>
</main>
