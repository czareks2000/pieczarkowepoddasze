<main>
  <div class="container">
    <div class="row">
      <div class="col-12">
          <article class="article">
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>Nazwa</th>
                    <th>Opis</th>
                    <th>Właściciel</th>
                    <th>Akcja</th>
                  </tr>
                </thead>
                <tbody>
                  <?php

                    require_once"connect.php";

                    $conn = @new MySQLi($host, $db_user, $db_password, $db_name);


                    if($conn->connect_errno!=0)
                    {
                      echo "Error: ".$conn->connect_errno;
                    }
                    else
                    {
                      mysqli_set_charset($conn,"utf8");

                      $sql = "SELECT * FROM planszowki";

                      $result = $conn->query($sql);


                      while($row = $result->fetch_assoc())
                      {
                        echo'<tr>';
                        echo'  <td>'.$row["nazwa"].'</td>';
                        echo'  <td>'.$row["opis"].'</td>';
                        echo'  <td>'.$row["wlasciciel"].'</td>';
                        echo'  <td>';
                        echo'   <form action="deleteGame.php" method="post">';
                        echo'    <input type="hidden" value="'.$row["nazwa"].'" name="name">';
                        echo'    <input type="hidden" value="'.$row["wlasciciel"].'" name="owner">';
                        echo'    <button type="submit" class="btn btn-secondary btn-sm">Usuń</button>';
                        echo'   </form>';
                        echo'  </td>';
                        echo'</tr>';
                      }


                      $result->close();
                      $conn->close();
                    }

                  ?>
                </tbody>
              </table>
            </div>
          </article>

          <article class="article">
            <h2>Dodaj nową planszówkę</h2>
              <form action="addGame.php" method="post">
                <div class="form-group row">
                  <label for="nowe-imie" class="col-lg-1 offset-lg-2 col-form-label">Nazwa</label>
                  <div class="col-lg-7">
                    <input class="form-control" value="<?php if(isset($_SESSION["graNazwa"])) echo $_SESSION["graNazwa"]?>" type="text" name="nazwa" id="nowe-imie">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nowe-nazwisko" class="col-lg-1 offset-lg-2 col-form-label">Opis</label>
                  <div class="col-lg-7">
                    <input class="form-control" value="<?php if(isset($_SESSION["graOpis"])) echo $_SESSION["graOpis"]?>" type="text" name="opis" id="nowe-nazwisko">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nowy-wlasciciel" class="col-lg-1 offset-lg-2 col-form-label">Właściciel</label>
                  <div class="col-lg-7">
                    <input class="form-control" value="<?php if(isset($_SESSION["graWlasciciel"])) echo $_SESSION["graWlasciciel"]?>" type="text"  name="wlasciciel" id="nowy-wlasciciel">
                  </div>
                </div>

              <div class="offset-lg-4 col-lg-4">
                <button type="submit" class="btn btn-secondary btn-block">Dodaj</button>
              </div>
            </form>
            <div class="error"><?php
              if(isset($_SESSION["blad"]))
              {
                echo $_SESSION["blad"];
              }
            ?></div>
          </article>

          <article class="article">
            <h2>Edytuj planszówkę</h2>
              <form action="editGame.php" method="post">
                <div class="form-group row">
                  <label class="col-form-label offset-lg-2 col-lg-1" for="inlineFormCustomSelect">Wybierz:</label>
                  <div class="col-lg-7">
                    <select class="custom-select" name="id" id="inlineFormCustomSelect">
                      <?php

                        require_once"connect.php";

                        $conn = @new MySQLi($host, $db_user, $db_password, $db_name);


                        if($conn->connect_errno!=0)
                        {
                          echo "Error: ".$conn->connect_errno;
                        }
                        else
                        {
                          mysqli_set_charset($conn,"utf8");

                          $sql = "SELECT * FROM planszowki";

                          $result = $conn->query($sql);

                          while($row = $result->fetch_assoc())
                          {
                            echo'  <option value="'.$row["id"].'">'.$row["nazwa"].'</option>';
                          }

                          $result->close();
                          $conn->close();
                        }

                      ?>
                     </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nowe-imie" class="col-lg-1 offset-lg-2 col-form-label">Nazwa</label>
                  <div class="col-lg-7">
                    <input class="form-control"type="text" name="nazwa" id="nowe-imie">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nowe-nazwisko" class="col-lg-1 offset-lg-2 col-form-label">Opis</label>
                  <div class="col-lg-7">
                    <input class="form-control" type="text" name="opis" id="nowe-nazwisko">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="nowy-wlasciciel" class="col-lg-1 offset-lg-2 col-form-label">Właściciel</label>
                  <div class="col-lg-7">
                    <input class="form-control" type="text"  name="wlasciciel" id="nowy-wlasciciel">
                  </div>
                </div>

              <div class="offset-lg-4 col-lg-4">
                <button type="submit" class="btn btn-secondary btn-block">Zastosuj zmiany</button>
              </div>
            </form>
          </article>


        </div>
      </div>
    </div>
  </div>
</main>
