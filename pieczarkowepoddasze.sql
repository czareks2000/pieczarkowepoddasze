-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 14 Lis 2018, 14:22
-- Wersja serwera: 10.1.36-MariaDB
-- Wersja PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `pieczarkowepoddasze`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `czlonkowie`
--

CREATE TABLE `czlonkowie` (
  `imie` text COLLATE utf8_polish_ci NOT NULL,
  `nazwisko` text COLLATE utf8_polish_ci NOT NULL,
  `podpis` text COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  `email` text COLLATE utf8_polish_ci NOT NULL,
  `zdjecie` text COLLATE utf8_polish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `czlonkowie`
--

INSERT INTO `czlonkowie` (`imie`, `nazwisko`, `podpis`, `opis`, `email`, `zdjecie`, `id`) VALUES
('Cezary', 'Stachurski', 'Pieczarek, Założyciel, Whisky Drinker', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, corrupti, reprehenderit. Provident architecto alias excepturi corporis sunt sed ut vero quos labore deleniti est dicta, aut nostrum blanditiis magni commodi.', 'czareks20006@gmail.com', 'img/members/stachurski.png', 1),
('Szymon', 'Siemieniuk', 'sPAGINOvAGINO, Współzałożyciel, Ulaniec', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, corrupti, reprehenderit. Provident architecto alias excepturi corporis sunt sed ut vero quos labore deleniti est dicta, aut nostrum blanditiis magni commodi.', 'szymon.siemieniuk26@gmail.com', 'img/members/siemieniuk.png', 2),
('Gabriel', 'Olszewski', 'Skunxik, SupremeAnimuBoi, Artysta-Malarz', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, corrupti, reprehenderit. Provident architecto alias excepturi corporis sunt sed ut vero quos labore deleniti est dicta, aut nostrum blanditiis magni commodi.', 'gabijuo@gmail.com', 'img/members/olszewski.png', 3),
('Łukasz', 'Sinielnikow', 'LuksoPL, Towarzysz, Russian lover', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, corrupti, reprehenderit. Provident architecto alias excepturi corporis sunt sed ut vero quos labore deleniti est dicta, aut nostrum blanditiis magni commodi.', 'lukasz@example.com', 'img/members/sinielnikow.png', 4),
('Juliusz', 'Odachowski', 'Dranakuyek, Bombiarz, Pro graczyk', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, corrupti, reprehenderit. Provident architecto alias excepturi corporis sunt sed ut vero quos labore deleniti est dicta, aut nostrum blanditiis magni commodi.', 'julek@example.com', 'img/members/odachowski.png', 5),
('Piotr', 'Bursa', 'GrzmocicielPL, Norweski Żołnierz, Jebacz JK', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, corrupti, reprehenderit. Provident architecto alias excepturi corporis sunt sed ut vero quos labore deleniti est dicta, aut nostrum blanditiis magni commodi.', 'piotr@example.com', 'img/members/bursa.png', 6),
('Kamil', 'Suszczyński', 'Mr.Shark, Smartass, Shounen boy', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam, corrupti, reprehenderit. Provident architecto alias excepturi corporis sunt sed ut vero quos labore deleniti est dicta, aut nostrum blanditiis magni commodi.', 'kamil@example.com', 'img/members/suszczynski.png', 7);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `planszowki`
--

CREATE TABLE `planszowki` (
  `nazwa` text COLLATE utf8_polish_ci NOT NULL,
  `opis` text COLLATE utf8_polish_ci NOT NULL,
  `wlasciciel` text COLLATE utf8_polish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `planszowki`
--

INSERT INTO `planszowki` (`nazwa`, `opis`, `wlasciciel`, `id`) VALUES
('Kartel(Monopoly)', 'kto nie zna Monopoly?', 'Cezary Stachurski', 1),
('Bingo', 'ulubieniec Norweskiego Żołnierza', 'Cezary Stachurski', 2),
('Jenga', 'ocena: 9/11', 'Cezary Stachurski', 3),
('Załóżmy się', 'wersja 18-stkowa', 'Cezary Stachurski', 4),
('Twister', 'try not to die spinning', 'Cezary Stachurski', 5),
('Bierki', '\"ale mi się łapy trzęsą\"', 'Cezary Stachurski', 6),
('Mikado', 'chińskie bierki', 'Cezary Stachurski', 7),
('Karty', 'nie służą do grania w Makao', 'Cezary Stachurski', 8),
('Memory', 'zapamiętywanie', 'Cezary Stachurski', 9),
('Ruletka', 'na kase z monopoly', 'Cezary Stachurski', 10),
('Piłkarzyki', 'na 2 osoby', 'Cezary Stachurski', 11),
('Małpy spadają szybko', 'naprawde szybko', 'Cezary Stachurski', 12),
('UNO', 'jedyne słuszne Makao', 'Cezary Stachurski', 13),
('Scrabble', 'ptaki nie istnieją', 'Szymon Siemieniuk', 14),
('Koejka', 'WASZA kolej', 'Szymon Siemieniuk', 15),
('Taboo', 'placeholder', 'Szymon Siemieniuk', 16),
('Szachy', 'szach mat idziesz w piach', 'Szymon Siemieniuk', 17),
('Czarne Historie', 'czarne', 'Szymon Siemieniuk', 18);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` text COLLATE utf8_polish_ci NOT NULL,
  `password` text COLLATE utf8_polish_ci NOT NULL,
  `name` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `user`, `password`, `name`) VALUES
(1, 'czareks2000', 'czareks123', 'Cezary');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zdjecia`
--

CREATE TABLE `zdjecia` (
  `podpis` text COLLATE utf8_polish_ci NOT NULL,
  `regular` text COLLATE utf8_polish_ci NOT NULL,
  `fullsize` text COLLATE utf8_polish_ci NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zdjecia`
--

INSERT INTO `zdjecia` (`podpis`, `regular`, `fullsize`, `id`) VALUES
('Poddasze', 'img/regular/attic1.png', 'img/full-size/attic1.png', 1),
('Poddasze', 'img/regular/attic2.png', 'img/full-size/attic2.png', 2),
('Poddasze', 'img/regular/attic3.png', 'img/full-size/attic3.png', 3),
('Poddasze', 'img/regular/attic4.png', 'img/full-size/attic4.png', 4),
('Poddasze', 'img/regular/attic5.png', 'img/full-size/attic5.png', 5),
('Poddasze', 'img/regular/attic6.png', 'img/full-size/attic6.png', 6),
('Poddasze', 'img/regular/attic7.png', 'img/full-size/attic7.png', 7);

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `czlonkowie`
--
ALTER TABLE `czlonkowie`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `planszowki`
--
ALTER TABLE `planszowki`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `zdjecia`
--
ALTER TABLE `zdjecia`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `czlonkowie`
--
ALTER TABLE `czlonkowie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT dla tabeli `planszowki`
--
ALTER TABLE `planszowki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT dla tabeli `zdjecia`
--
ALTER TABLE `zdjecia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
